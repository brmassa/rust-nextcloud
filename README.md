<img src="public/todonosso-complete.png" alt="TodoNosso Logo" height="124">


# TodoNosso

Online office hub (files, calendar, tasks, photos) for people and companies.

## Install

### Compose

If you have Docker (or nerdctl or podman), it can follow the instructions of a file to install and run the app in your machine. Create a file `.docker-compose.yml` with the content:

```yaml
version: '3.4'

services:
  todonosso:
    image: todonosso:latest
    ports:
      - 3000:3000
```

```sh
docker compose up
```

Now access the site using `http://localhost:3000` and _voi lá_!

To stop, call:

```sh
docker compose up
```

## License

The project is proudly FLOSS (free libre open source). Check the [license](LICENSE) details.

## Status

- main: [![pipeline status](https://gitlab.com/brmassa/todonosso/badges/main/pipeline.svg)](https://gitlab.com/brmassa/todonosso/commits/main)

[<img src="https://storage.ko-fi.com/cdn/brandasset/kofi_button_blue.png?_gl=1*taksku*_ga*NDYwNzM3MTkxLjE2Nzg3NzYwMjg.*_ga_M13FZ7VQ2C*MTY3ODc3NjAyOC4xLjEuMTY3ODc3NjA5OS41My4wLjA." alt="TodoNosso Logo" height="50">](https://ko-fi.com/m4554)
