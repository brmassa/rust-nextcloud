// use std::fs;
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    pub http: ConfigHTTP,
    pub jwt: ConfigJWT,
    pub storage: ConfigStorage,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ConfigHTTP {
    pub host: String,
    pub http_port: u16,
    pub enable_https: bool,
    pub https_port: u16,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigJWT {
    pub secret: String,
    pub expires: i64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigStorage {
    pub path: String,
}

// impl Config {
//     /// Parse configuration file.
//     pub fn parse(path: &str) -> anyhow::Result<Self> {
//         let config_str = fs::read_to_string(path)?;

//         let config = toml::from_str(&config_str)?;

//         Ok(config)
//     }
// }
