use libloading::{Library, Symbol};
use std::{fs, path};

pub mod files;

/// Basic Plugin trait. All plugins must implement this.
///
/// Libraries that do not implement this will be skipped.
pub trait IPlugin {
    fn title(&self) -> String;
    fn init(&self);
}

pub(crate) unsafe fn load_plugins(plugin_dir: &path::Path) {
    let plugin_path = path::Path::new(&plugin_dir);

    if let Ok(plugin_files) = fs::read_dir(plugin_path) {
        for file in plugin_files {
            let file_path = file.expect("Failed to read plugin file").path();
            let extension = file_path.extension();
            if extension.is_some() && extension.unwrap().to_str() == Some(library_extension()) {
                let lib = match Library::new(&file_path) {
                    Ok(x) => x,
                    Err(x) => panic!("{}", x),
                };

                tracing::info!("plugin: {}", file_path.to_str().unwrap());
                unsafe {
                    let init: Symbol<fn()> = match lib.get(b"init") {
                        Ok(x) => x,
                        Err(x) => panic!("plugin failed to load: {}", x),
                    };
                    init();
                    let title: Symbol<fn() -> String> = match lib.get(b"title") {
                        Ok(x) => x,
                        Err(x) => panic!("plugin failed to load: {}", x),
                    };
                    let t = title();
                    tracing::info!("Plugin loaded {}!", t);
                };
            }
        }
    }
}

fn library_extension() -> &'static str {
    #[cfg(windows)]
    return "dll";
    return "so";
}
