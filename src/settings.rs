use askama::Template;
use axum::{
    http::StatusCode,
    response::{Html, IntoResponse, Response},
    routing::get,
    Router,
};

pub fn routes() -> Router {
    Router::new()
        .route("/settings", get(settings))
        .route("/settings/main", get(settings))
}

#[derive(Template)]
#[template(path = "settings.html")]
struct IndexTemplate {
    title: String,
}

async fn settings() -> impl IntoResponse {
    let template = IndexTemplate {
        title: String::from("TodoNosso"),
    };
    HtmlTemplate(template)
}

struct HtmlTemplate<T>(T);
impl<T> IntoResponse for HtmlTemplate<T>
where
    T: Template,
{
    fn into_response(self) -> Response {
        match self.0.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {}", err),
            )
                .into_response(),
        }
    }
}
