use askama::Template;
use axum::{
    http::StatusCode,
    response::{Html, IntoResponse, Response},
    routing::get,
    Router,
};
use std::{net, path};
use tower_http::services::ServeDir;

use plugins::files;

mod config;
mod plugins;
mod settings;

#[tokio::main]
async fn main() {
    // Include the tracing in the default console
    tracing_subscriber::fmt::init();
    tracing::info!("Welcome to TodoNosso!");

    // Load every plugin
    unsafe {
        plugins::load_plugins(path::Path::new("plugins"));
    }

    let routes = Router::new()
        .route("/", get(index))
        .route("/api/files", get(crate::files::files::handle_files))
        .nest_service("/public", ServeDir::new("public"))
        .merge(settings::routes());

    let addr = net::SocketAddr::from(([0, 0, 0, 0], 3000));
    tracing::info!("listening on http://{}", addr);
    axum::Server::bind(&addr)
        .serve(routes.into_make_service())
        .await
        .unwrap();
}

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {
    title: String,
}

async fn index() -> impl IntoResponse {
    let template = IndexTemplate {
        title: String::from("TodoNosso"),
    };
    HtmlTemplate(template)
}

struct HtmlTemplate<T>(T);
impl<T> IntoResponse for HtmlTemplate<T>
where
    T: Template,
{
    fn into_response(self) -> Response {
        match self.0.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {}", err),
            )
                .into_response(),
        }
    }
}
