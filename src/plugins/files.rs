pub mod files {

    use axum::{http::StatusCode, response::IntoResponse};
    use serde::Serialize;
    use std::{fs, path::PathBuf};

    #[derive(Serialize)]
    struct File {
        name: String,
        path: String,
        is_dir: bool,
    }

    async fn list_files(path: PathBuf) -> Vec<File> {
        let mut files = Vec::new();

        if let Ok(entries) = fs::read_dir(path) {
            for entry in entries {
                if let Ok(entry) = entry {
                    let file_type = entry.file_type().unwrap();
                    let file_name = entry.file_name().to_string_lossy().to_string();
                    let file_path = entry.path().to_string_lossy().to_string();

                    let file = File {
                        name: file_name,
                        path: file_path,
                        is_dir: file_type.is_dir(),
                    };

                    files.push(file);
                }
            }
        }

        return files;
    }

    pub async fn handle_files() -> Result<impl IntoResponse, StatusCode> {
        let path = PathBuf::from("files");
        let files = list_files(path).await;
        return Ok((StatusCode::OK, axum::Json(files)));
    }
}
