use std::env;
use std::fs;
use std::path::Path;
use std::path::PathBuf;
use std::time::{SystemTime, UNIX_EPOCH};

/// move all plugins to "plugins" folder
fn main() {
    let target = if cfg!(debug_assertions) {
        "debug"
    } else {
        "release"
    };

    let now = SystemTime::now();
    let timestamp = now.duration_since(UNIX_EPOCH).unwrap();
    println!("start building {} {:?}", target, timestamp);
    // Get the root directory
    let root_dir = PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap())
        .join("target")
        .join(target);
    println!("{}", root_dir.display());

    // Set the workspace directory to the root directory
    let workspace_dir = root_dir.clone();

    // Set the plugins directory path
    let plugins_dir_path = Path::new(&root_dir).join("plugins");

    // Create the plugins directory if it does not exist
    if !plugins_dir_path.exists() {
        fs::create_dir(&plugins_dir_path).unwrap();
    }
    
    // Get a list of all DLL files in the workspace directory
    let dll_files = workspace_dir
        .read_dir()
        .unwrap()
        .filter_map(Result::ok)
        .filter(|entry| {
            entry
                .path()
                .extension()
                .map(|ext| ext == library_extension())
                .unwrap_or(false)
        });

    // Move each DLL file to the plugins directory
    for dll_file in dll_files {
        let path = dll_file.path();
        let file_name = path.file_name().unwrap();
        let dest_path = plugins_dir_path.join(file_name);

        fs::rename(dll_file.path(), dest_path).unwrap();
    }

    let now = SystemTime::now();
    let timestamp = now.duration_since(UNIX_EPOCH).unwrap();
    println!("finished {:?}", timestamp);
}

fn library_extension() -> &'static str {
    #[cfg(windows)]
    return "dll";
    return "so";
}
