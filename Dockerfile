# build stage
FROM rust:latest AS builder

# TRICK tor reuse of the Docker cache
# RUN USER=root cargo new --bin todonosso
WORKDIR /todonosso
# COPY ./Cargo.lock ./Cargo.lock
# COPY ./Cargo.toml ./Cargo.toml
# RUN touch src/main.rs 
# RUN cargo build --release
# RUN rm src/*.rs

#build
COPY . .
RUN cargo build --release --workspace

# use a clean image for the app
FROM ubuntu:22.10 AS runtime
WORKDIR /todonosso
COPY --from=builder /todonosso/target/release/todonosso .
COPY --from=builder /todonosso/target/release/plugins ./plugins/
COPY ./public ./public/
COPY ./templates ./templates/
# TODO: test the files folder
COPY ./Cargo.toml ./files/Cargo.toml
ENTRYPOINT ["./todonosso"]
EXPOSE 3000
