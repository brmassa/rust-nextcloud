/// Basic Plugin trait. All plugins must implement this.
///
/// Libraries that do not implement this will be skipped.
pub trait IPlugin {
    fn title(&self) -> String;
    fn init(&self);
}

#[derive(Debug, Clone)]
struct Calendar {}

impl IPlugin for Calendar {
    #[no_mangle]
    fn title(&self) -> String {
        String::from("calendar")
    }

    #[no_mangle]
    fn init(&self) {
        tracing_subscriber::fmt::init();
        tracing::info!("Plugin loaded {}!", self.title());
    }
}
